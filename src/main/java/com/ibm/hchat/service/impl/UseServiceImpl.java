package com.ibm.hchat.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibm.hchat.mapper.TbUserMapper;
import com.ibm.hchat.pojo.TbUser;
import com.ibm.hchat.service.UserService;
@Service
@Transactional
public class UseServiceImpl implements UserService{
   @Autowired
   private TbUserMapper tbUserMapper;
	@Override
	public List<TbUser> findAll() {
		List<TbUser> tbUserList = tbUserMapper.selectByExample(null);
		return tbUserList;
	}

}
