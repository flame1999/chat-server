package com.ibm.hchat.service;

import com.ibm.hchat.pojo.TbUser;


import java.util.List;

public interface UserService {
    
	/**返回所有的TbUser
	 * @return
	 */
	List<TbUser> findAll(); 		
}
