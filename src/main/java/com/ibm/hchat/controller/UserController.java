package com.ibm.hchat.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ibm.hchat.pojo.TbUser;
import com.ibm.hchat.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	@Autowired 
	private UserService userService;
	@RequestMapping("/findAll")
	public List<TbUser> findAll(){
		return userService.findAll();
		
	}

}
